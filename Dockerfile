FROM java:8
MAINTAINER <kun_ja@163>
COPY target/demo-0.0.1-SNAPSHOT.jar /app.jar

CMD ["--server.port=8080"]
EXPOSE [8080, 6379]

ENTRYPOINT["java","-jar", "/app.jar"]

