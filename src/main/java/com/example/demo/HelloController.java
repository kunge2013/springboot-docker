package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 玄策
 * @version 1.0
 * @date 2021/2/22 12:51 下午
 * @description:
 */
@RestController
public class HelloController {


    @RequestMapping(method = RequestMethod.GET, value = "/test")
    public String test() {
        return "hello world";
    }
}
